# envsimg

**The one and only place to find the envs lounge welcome image.**

# What is the lounge?

The absolute ***best*** Matrix room.

# I want to fork this project. Are there any guidelines?!

Yes. [Check the wiki.](https://git.envs.net/bqfls/envsimg/wiki/Forking-requirements)